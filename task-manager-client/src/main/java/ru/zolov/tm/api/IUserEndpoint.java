package ru.zolov.tm.api;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.3.5
 * 2020-02-05T15:54:32.501+03:00
 * Generated source version: 3.3.5
 *
 */
@WebService(targetNamespace = "http://api.tm.zolov.ru/", name = "IUserEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface IUserEndpoint {

    @WebMethod
    @Action(input = "http://api.tm.zolov.ru/IUserEndpoint/isRolesAllowedRequest", output = "http://api.tm.zolov.ru/IUserEndpoint/isRolesAllowedResponse", fault = {@FaultAction(className = EmptyRepositoryException_Exception.class, value = "http://api.tm.zolov.ru/IUserEndpoint/isRolesAllowed/Fault/EmptyRepositoryException"), @FaultAction(className = EmptyStringException_Exception.class, value = "http://api.tm.zolov.ru/IUserEndpoint/isRolesAllowed/Fault/EmptyStringException"), @FaultAction(className = SQLException_Exception.class, value = "http://api.tm.zolov.ru/IUserEndpoint/isRolesAllowed/Fault/SQLException"), @FaultAction(className = AccessForbiddenException_Exception.class, value = "http://api.tm.zolov.ru/IUserEndpoint/isRolesAllowed/Fault/AccessForbiddenException"), @FaultAction(className = CloneNotSupportedException_Exception.class, value = "http://api.tm.zolov.ru/IUserEndpoint/isRolesAllowed/Fault/CloneNotSupportedException")})
    @RequestWrapper(localName = "isRolesAllowed", targetNamespace = "http://api.tm.zolov.ru/", className = "ru.zolov.tm.api.IsRolesAllowed")
    @ResponseWrapper(localName = "isRolesAllowedResponse", targetNamespace = "http://api.tm.zolov.ru/", className = "ru.zolov.tm.api.IsRolesAllowedResponse")
    @WebResult(name = "return", targetNamespace = "") boolean isRolesAllowed(

        @WebParam(name = "session", targetNamespace = "") ru.zolov.tm.api.Session session,
        @WebParam(name = "roleType", targetNamespace = "") java.util.List<ru.zolov.tm.api.RoleType> roleType
    ) throws EmptyRepositoryException_Exception, EmptyStringException_Exception, SQLException_Exception, AccessForbiddenException_Exception, CloneNotSupportedException_Exception;

    @WebMethod
    @Action(input = "http://api.tm.zolov.ru/IUserEndpoint/registerNewUserRequest", output = "http://api.tm.zolov.ru/IUserEndpoint/registerNewUserResponse", fault = {@FaultAction(className = EmptyRepositoryException_Exception.class, value = "http://api.tm.zolov.ru/IUserEndpoint/registerNewUser/Fault/EmptyRepositoryException"), @FaultAction(className = EmptyStringException_Exception.class, value = "http://api.tm.zolov.ru/IUserEndpoint/registerNewUser/Fault/EmptyStringException"), @FaultAction(className = SQLException_Exception.class, value = "http://api.tm.zolov.ru/IUserEndpoint/registerNewUser/Fault/SQLException"), @FaultAction(className = UserExistException_Exception.class, value = "http://api.tm.zolov.ru/IUserEndpoint/registerNewUser/Fault/UserExistException")})
    @RequestWrapper(localName = "registerNewUser", targetNamespace = "http://api.tm.zolov.ru/", className = "ru.zolov.tm.api.RegisterNewUser")
    @ResponseWrapper(localName = "registerNewUserResponse", targetNamespace = "http://api.tm.zolov.ru/", className = "ru.zolov.tm.api.RegisterNewUserResponse")
    @WebResult(name = "return", targetNamespace = "") ru.zolov.tm.api.User registerNewUser(

        @WebParam(name = "login", targetNamespace = "") java.lang.String login,
        @WebParam(name = "password", targetNamespace = "") java.lang.String password
    ) throws EmptyRepositoryException_Exception, EmptyStringException_Exception, SQLException_Exception, UserExistException_Exception;

    @WebMethod
    @Action(input = "http://api.tm.zolov.ru/IUserEndpoint/updateUserPasswordRequest", output = "http://api.tm.zolov.ru/IUserEndpoint/updateUserPasswordResponse", fault = {@FaultAction(className = AccessForbiddenException_Exception.class, value = "http://api.tm.zolov.ru/IUserEndpoint/updateUserPassword/Fault/AccessForbiddenException"), @FaultAction(className = CloneNotSupportedException_Exception.class, value = "http://api.tm.zolov.ru/IUserEndpoint/updateUserPassword/Fault/CloneNotSupportedException"), @FaultAction(className = UserNotFoundException_Exception.class, value = "http://api.tm.zolov.ru/IUserEndpoint/updateUserPassword/Fault/UserNotFoundException"), @FaultAction(className = EmptyRepositoryException_Exception.class, value = "http://api.tm.zolov.ru/IUserEndpoint/updateUserPassword/Fault/EmptyRepositoryException"), @FaultAction(className = EmptyStringException_Exception.class, value = "http://api.tm.zolov.ru/IUserEndpoint/updateUserPassword/Fault/EmptyStringException"), @FaultAction(className = SQLException_Exception.class, value = "http://api.tm.zolov.ru/IUserEndpoint/updateUserPassword/Fault/SQLException")})
    @RequestWrapper(localName = "updateUserPassword", targetNamespace = "http://api.tm.zolov.ru/", className = "ru.zolov.tm.api.UpdateUserPassword")
    @ResponseWrapper(localName = "updateUserPasswordResponse", targetNamespace = "http://api.tm.zolov.ru/", className = "ru.zolov.tm.api.UpdateUserPasswordResponse")
    @WebResult(name = "return", targetNamespace = "") ru.zolov.tm.api.User updateUserPassword(

        @WebParam(name = "session", targetNamespace = "") ru.zolov.tm.api.Session session,
        @WebParam(name = "newPassword", targetNamespace = "") java.lang.String newPassword
    ) throws AccessForbiddenException_Exception, CloneNotSupportedException_Exception, UserNotFoundException_Exception, EmptyRepositoryException_Exception, EmptyStringException_Exception, SQLException_Exception;
}
