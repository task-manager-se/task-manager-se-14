package ru.zolov.tm.api;

import javax.persistence.EntityManagerFactory;
import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {

  @NotNull EntityManagerFactory getEntityManagerFactory();

  @NotNull IProjectService getProjectService();

  @NotNull ITaskService getTaskService();

  @NotNull IUserService getUserService();

  @NotNull IDomainService getDomainService();

  @NotNull ISessionService getSessionService();
}
