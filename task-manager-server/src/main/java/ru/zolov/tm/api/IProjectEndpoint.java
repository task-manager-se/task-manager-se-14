package ru.zolov.tm.api;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.dto.ProjectDto;
import ru.zolov.tm.dto.SessionDto;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

@WebService
public interface IProjectEndpoint {

  @NotNull @WebMethod ProjectDto createNewProject(
      @NotNull @WebParam(name = "sessionDTO") SessionDto sessionDTO,
      @NotNull @WebParam(name = "projectName") String projectName,
      @NotNull @WebParam(name = "projectDescription") String projectDescription,
      @NotNull @WebParam(name = "dateOfStart") String dateOfStart,
      @NotNull @WebParam(name = "dateOfFinish") String dateOfFinish
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, ParseException, SQLException, EmptyRepositoryException;

  @NotNull @WebMethod List<ProjectDto> findAllProjectByUserId(
      @NotNull @WebParam(name = "sessionDTO") SessionDto sessionDTO
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException, SQLException;

  @Nullable @WebMethod ProjectDto findProjectById(
      @NotNull @WebParam(name = "sessionDTO") SessionDto sessionDTO,
      @NotNull @WebParam(name = "projectId") String projectId
  ) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @WebMethod void removeProjectById(
      @NotNull @WebParam(name = "sessionDTO") SessionDto sessionDTO,
      @NotNull @WebParam(name = "projectId") String projectId
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException, SQLException;

  @WebMethod void updateProject(
      @NotNull @WebParam(name = "sessionDTO") SessionDto sessionDTO,
      @NotNull @WebParam(name = "projectId") String projectId,
      @NotNull @WebParam(name = "projectName") String projectName,
      @NotNull @WebParam(name = "projectDescription") String projectDescription,
      @NotNull @WebParam(name = "dateOfStart") String start,
      @NotNull @WebParam(name = "dateOfFinish") String finish
  ) throws AccessForbiddenException, CloneNotSupportedException, ParseException, EmptyRepositoryException, SQLException, EmptyStringException;


  @NotNull @WebMethod List<ProjectDto> findProjectByPartOfTheName(
      @NotNull @WebParam(name = "sessionDTO") SessionDto sessionDTO,
      @NotNull @WebParam(name = "partOfTheName") String partOfTheName
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyRepositoryException, EmptyStringException, SQLException;
}
