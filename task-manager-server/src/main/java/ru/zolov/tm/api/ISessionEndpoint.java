package ru.zolov.tm.api;

import java.sql.SQLException;
import java.text.ParseException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.dto.SessionDto;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserNotFoundException;

@WebService
public interface ISessionEndpoint {

  @Nullable @WebMethod SessionDto openSession(
      @NotNull @WebParam(name = "login") String login,
      @NotNull @WebParam(name = "password") String password
  ) throws EmptyStringException, UserNotFoundException, ParseException, SQLException, EmptyRepositoryException;

  @WebMethod void closeSesson(@NotNull @WebParam(name = "sessionDTO") SessionDto sessionDTO) throws AccessForbiddenException, CloneNotSupportedException, EmptyRepositoryException, SQLException, EmptyStringException;
}
