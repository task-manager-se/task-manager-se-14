package ru.zolov.tm.api;

import java.util.Date;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ITerminalService {

  String nextLine();

  Integer nextInt();

}
