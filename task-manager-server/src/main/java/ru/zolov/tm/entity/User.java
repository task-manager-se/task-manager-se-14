package ru.zolov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.enumerated.RoleType;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_user")
public class User extends AbstractEntity {
    @NotNull
    @Column(unique = true)
    private String login = "";
    @NotNull
    @Column(name = "password_hash")
    private String passwordHash = "";
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private RoleType role = RoleType.USER;
}
