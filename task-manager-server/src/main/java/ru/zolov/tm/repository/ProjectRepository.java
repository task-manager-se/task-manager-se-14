package ru.zolov.tm.repository;

import java.sql.SQLException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class ProjectRepository {

  @NotNull final EntityManager entityManager;

  public ProjectRepository(EntityManager entityManager) {
    this.entityManager = entityManager;
  }

  public @Nullable Project findOneByUserId(
      @NotNull final String userId,
      @NotNull final String id
  ) throws SQLException, EmptyStringException {
    return (Project)entityManager.createQuery("SELECT p FROM Project p WHERE user_id = ?1 AND id = ?2").setParameter(1, userId)
        .getSingleResult();
  }

  public @NotNull List<Project> findAllByUserId(@NotNull final String userId) throws SQLException, EmptyStringException {
    return entityManager.createQuery("SELECT p FROM Project p WHERE user_id = ?1").setParameter(1, userId).getResultList();
  }

  public void removeAllByUserId(@NotNull final String userId) throws SQLException, EmptyRepositoryException, EmptyStringException {
    List<Project> list = findAllByUserId(userId);
    for (Project project : list) {
      entityManager.remove(project);
    }
  }

  public @NotNull List<Project> findAll() throws EmptyRepositoryException, SQLException, EmptyStringException {
    return entityManager.createQuery("SELECT p FROM Project p", Project.class).getResultList();
  }

  public @Nullable Project findOne(@NotNull final String id) throws EmptyStringException, EmptyRepositoryException, SQLException {
    return entityManager.find(Project.class, id);
  }

  public void persist(@NotNull final Project entity) throws SQLException, EmptyStringException {
    entityManager.persist(entity);
  }

  public void merge(@NotNull final Project entity) throws EmptyStringException, EmptyRepositoryException, SQLException {
    entityManager.merge(entity);
  }

  public void remove(@NotNull final String id) throws SQLException {
    entityManager.remove(id);
  }

  public void removeAll() throws SQLException {
    entityManager.createQuery("DELETE FROM Project p", Project.class).executeUpdate();
  }

  public @NotNull List<Project> findProjectByPartOfTheName(
      @NotNull final String userId,
      @NotNull final String partOfTheName
  ) throws SQLException, EmptyStringException {
    Query query = entityManager.createQuery("SELECT p FROM Project p WHERE p.user.id = ?1 AND (p.name LIKE CONCAT('%', ?2, '%')"
        + " OR p.description LIKE CONCAT ('%', ?3, '%'))", Project.class);
    query.setParameter("1", userId);
    query.setParameter("2", partOfTheName);
    query.setParameter("3", partOfTheName);
    return query.getResultList();
  }
}
