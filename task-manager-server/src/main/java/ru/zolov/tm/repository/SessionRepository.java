package ru.zolov.tm.repository;

import java.sql.SQLException;
import java.util.List;
import javax.persistence.EntityManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class SessionRepository {

  @NotNull final EntityManager entityManager;

  public SessionRepository(final EntityManager entityManager) {
    this.entityManager = entityManager;
  }

  public @NotNull List<Session> findAll(@NotNull final String userId) throws EmptyRepositoryException, SQLException, EmptyStringException {
    return entityManager.createQuery("SELECT s FROM Session s WHERE user.id = ?1", Session.class).setParameter(1, userId).getResultList();
  }

  public @Nullable Session findOne(@NotNull final String id) throws EmptyStringException, EmptyRepositoryException, SQLException {
    return entityManager.find(Session.class, id);
  }

  public void persist(@NotNull final Session entity) throws SQLException, EmptyStringException {
    entityManager.persist(entity);
  }

  public void merge(@NotNull final Session entity) throws EmptyStringException, EmptyRepositoryException, SQLException {
    entityManager.merge(entity);
  }

  public void remove(@NotNull final String id) throws SQLException {
    entityManager.remove(id);
  }

  public void removeAllByUserId(@NotNull final String userId) throws SQLException, EmptyRepositoryException, EmptyStringException {
    List<Session> list = findAll(userId);
    for (Session session : list) {
      entityManager.remove(session);
    }
  }
}
