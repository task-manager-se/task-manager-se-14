package ru.zolov.tm.repository;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class TaskRepository {

  EntityManager entityManager;

  public TaskRepository(EntityManager entityManager) {
    this.entityManager = entityManager;
  }

  public @NotNull List<Task> findAllByUserId(@NotNull final String userId) throws SQLException, EmptyStringException {
    return entityManager.createQuery("SELECT t FROM Task t WHERE user.id = ?1").setParameter(1, userId).getResultList();
  }

  public void removeAllByUserId(@NotNull final String userId) throws SQLException, EmptyStringException {
    List<Task> list = findAllByUserId(userId);
    for (@NotNull final Task task : list) {
      entityManager.remove(task);
    }
  }

  public @NotNull List<Task> findAllByProjectIdByUserId(
      @NotNull final String userId,
      @NotNull final String projectId
  ) throws SQLException, EmptyRepositoryException, EmptyStringException {
    return Collections
        .unmodifiableList(entityManager.createQuery("SELECT t FROM Task t WHERE user .id = ?1 AND project .id = ?2").setParameter(1, userId)
            .setParameter(2, projectId).getResultList());
  }

  public void removeAllTaskByProjectId(
      @NotNull final String projectId
  ) throws EmptyRepositoryException, SQLException, EmptyStringException {
    List<Task> list = findAllByProjectId(projectId);
    for (Task task : list) {
      entityManager.remove(task);
    }
  }

  public @NotNull List<Task> findAllByProjId(@NotNull final String projectId) throws EmptyRepositoryException, SQLException, EmptyStringException {
    return entityManager.createQuery("SELECT t FROM Task t WHERE project.id = ?1").setParameter(1, projectId).getResultList();
  }

  public @NotNull List<Task> findTaskByPartOfTheName(
      @NotNull final String userId,
      @NotNull final String partOfTheName
  ) throws EmptyRepositoryException, SQLException, EmptyStringException {
    Query query = entityManager.createQuery("SELECT t FROM Task t WHERE t.user.id = ?1 AND (t.name LIKE CONCAT('%', ?2, '%')"
        + " OR t.description LIKE CONCAT ('%', ?3, '%'))", Project.class);
    query.setParameter("1", userId);
    query.setParameter("2", partOfTheName);
    query.setParameter("3", partOfTheName);
    return query.getResultList();
  }

  public @NotNull List<Task> findAll() throws EmptyRepositoryException, SQLException, EmptyStringException {
    return entityManager.createQuery("SELECT t FROM Task t", Task.class).getResultList();
  }

  public @NotNull List<Task> findAllByProjectId(@NotNull final String projectId) throws EmptyRepositoryException, SQLException, EmptyStringException {
    return entityManager.createQuery("SELECT t FROM Task t where project.id = ?1", Task.class).setParameter(1, projectId).getResultList();
  }

  public @Nullable Task findOne(@NotNull final String id) throws EmptyStringException, EmptyRepositoryException, SQLException {
    return entityManager.createQuery("SELECT t FROM Task t WHERE id = ?1", Task.class).setParameter(1, id).getSingleResult();
  }

  public void persist(@NotNull final Task entity) throws SQLException, EmptyStringException {
    entityManager.persist(entity);
  }

  public void merge(@NotNull final Task entity) throws EmptyStringException, EmptyRepositoryException, SQLException {
    entityManager.merge(entity);
  }

  public void remove(@NotNull final String id) throws SQLException {
    entityManager.remove(id);
  }
}
