package ru.zolov.tm.repository;

import java.sql.SQLException;
import java.util.List;
import javax.persistence.EntityManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class UserRepository {

  @NotNull private final EntityManager entityManager;

  public UserRepository(EntityManager entityManager) {
    this.entityManager = entityManager;
  }

  public @NotNull List<User> findAll() throws EmptyRepositoryException, SQLException, EmptyStringException {
    return entityManager.createQuery("SELECT u FROM User u", User.class).getResultList();
  }

  public @Nullable User findByLogin(final @NotNull String login) throws EmptyStringException, EmptyRepositoryException, SQLException {
    return entityManager.createQuery("SELECT u FROM User u where u.login = :login", User.class).setParameter("login", login).getSingleResult();
  }

  public @Nullable User findById(@NotNull final String id) throws EmptyStringException, EmptyRepositoryException, SQLException {
    return entityManager.find(User.class, id);
  }

  public void persist(@NotNull final User entity) throws SQLException, EmptyStringException {
    entityManager.persist(entity);
  }

  public void merge(@NotNull final User entity) throws EmptyStringException, EmptyRepositoryException, SQLException {
    entityManager.merge(entity);
  }

  public void remove(@NotNull final String id) throws SQLException {
    entityManager.remove(id);
  }
}
