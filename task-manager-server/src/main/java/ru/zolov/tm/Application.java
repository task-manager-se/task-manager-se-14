package ru.zolov.tm;

import ru.zolov.tm.loader.Bootstrap;

public class Application {

  public static void main(String[] args) throws Exception {
    System.setProperty("javax.xml.bind.JAXBContextFactory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
    Bootstrap bootstrap = new Bootstrap();
    bootstrap.init();
  }
}
