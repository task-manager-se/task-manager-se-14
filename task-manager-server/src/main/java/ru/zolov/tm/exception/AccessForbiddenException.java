package ru.zolov.tm.exception;

public class AccessForbiddenException extends Exception {

  public AccessForbiddenException() {
    super("Access forbidden!");
  }
}
