package ru.zolov.tm.exception;

public class UserNotFoundException extends Exception {

  public UserNotFoundException() {
    super("UserDto not found!");
  }
}
