package ru.zolov.tm.exception;

public class SessionExpiredException extends Exception {

  public SessionExpiredException() {
    super("SessionDto has expired!");
  }
}
