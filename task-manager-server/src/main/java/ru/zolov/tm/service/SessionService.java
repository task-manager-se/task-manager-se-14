package ru.zolov.tm.service;

import java.sql.SQLException;
import javax.persistence.EntityManager;
import lombok.Cleanup;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.api.ISessionService;
import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.dto.SessionDto;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.SessionExpiredException;
import ru.zolov.tm.repository.SessionRepository;
import ru.zolov.tm.util.PropertyUtil;
import ru.zolov.tm.util.SignatureUtil;

public class SessionService extends AbstractService<Session> implements ISessionService {

  @NotNull private ServiceLocator serviceLocator;

  public SessionService(@NotNull final ServiceLocator serviceLocator) {
    this.serviceLocator = serviceLocator;
  }

  @Override public @NotNull Session open(final User user) throws EmptyStringException, SQLException {
    @Cleanup @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerFactory().createEntityManager();
    @NotNull SessionRepository sessionRepository = new SessionRepository(entityManager);

    @NotNull final Session session = new Session();
    @NotNull final String salt = PropertyUtil.SALT;
    @NotNull final Integer cycle = PropertyUtil.CYCLE;

    session.setUser(user);
    session.setTimestamp(System.currentTimeMillis());
    session.setSignature(SignatureUtil.sign(session, salt, cycle));
    entityManager.getTransaction().begin();
    sessionRepository.persist(session);
    entityManager.getTransaction().commit();
    return session;
  }

  @Override public void validate(Session session) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException, SQLException, SessionExpiredException {
    @NotNull final String salt = PropertyUtil.SALT;
    @NotNull final Integer cycle = PropertyUtil.CYCLE;
    if (session == null) throw new AccessForbiddenException();
    if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessForbiddenException();
    if (session.getTimestamp() == null) throw new SessionExpiredException();
    if (session.getUser() == null) throw new AccessForbiddenException();
    @Cleanup @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerFactory().createEntityManager();
    @NotNull SessionRepository sessionRepository = new SessionRepository(entityManager);
    @NotNull final Session temp = session.clone();
    if (temp == null) throw new AccessForbiddenException();
    @NotNull final String signatureSource = session.getSignature();
    temp.setSignature(null);
    @NotNull final String signatureTarget = SignatureUtil.sign(temp, salt, cycle);
    final boolean check = signatureSource.equals(signatureTarget);
    final boolean live = checkSessionIsAlive(session.getTimestamp());
    if (!check) throw new AccessForbiddenException();
    if (!live) throw new SessionExpiredException();
  }

  @Override public void close(Session session) throws SQLException {
    @Cleanup @NotNull final EntityManager entityManager = serviceLocator.getEntityManagerFactory().createEntityManager();
    @NotNull SessionRepository sessionRepository = new SessionRepository(entityManager);
    sessionRepository.remove(session.getId());

  }

  public boolean checkSessionIsAlive(@NotNull final Long sessionTimeStamp) {
    @NotNull final long sessionLiveTimeMs = PropertyUtil.SESSION_LIVETIME;
    @NotNull final long currentTimeMs = System.currentTimeMillis();
    return ((currentTimeMs - sessionTimeStamp) <= sessionLiveTimeMs);
  }

  @Override
  public Session transformDtoToSession(@NotNull SessionDto sessionDto, @NotNull User user) {
    @NotNull Session session = new Session();
    session.setId(sessionDto.getId());
    session.setSignature(sessionDto.getSignature());
    session.setTimestamp(sessionDto.getTimestamp());
    session.setUser(user);
    sessionDto.setRoleType(user.getRole());
    return session;
  }

  @Override
  public SessionDto transformSessionToDto(@NotNull final Session session, @NotNull final User user) {
    @NotNull SessionDto sessionDto = new SessionDto();
    sessionDto.setId(session.getId());
    sessionDto.setSignature(session.getSignature());
    sessionDto.setTimestamp(session.getTimestamp());
    sessionDto.setUserId(session.getUser().getId());
    return sessionDto;
  }
}
