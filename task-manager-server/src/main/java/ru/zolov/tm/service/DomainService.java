package ru.zolov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.api.IDomainService;
import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.dto.DomainDto;
import ru.zolov.tm.dto.SessionDto;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;

public class DomainService implements IDomainService {

  protected ServiceLocator serviceLocator;

  public DomainService(ServiceLocator serviceLocator) {
    this.serviceLocator = serviceLocator;
  }

  @SneakyThrows public void load(@NotNull SessionDto sessionDto, @NotNull final DomainDto domainDTO) {
    @NotNull final User user = serviceLocator.getUserService().findOneById(sessionDto.getUserId());
    @NotNull final Session session = serviceLocator.getSessionService().transformDtoToSession(sessionDto, user);
    serviceLocator.getProjectService().load(session, domainDTO.getProject());
    serviceLocator.getTaskService().load(session, domainDTO.getTask());
    serviceLocator.getUserService().load(session, domainDTO.getUser());

  }

  @SneakyThrows public void save(@NotNull SessionDto sessionDto, @NotNull final DomainDto domainDto) {
    @NotNull final User user = serviceLocator.getUserService().findOneById(sessionDto.getUserId());
    @NotNull final Session session = serviceLocator.getSessionService().transformDtoToSession(sessionDto, user);
    domainDto.setProject(serviceLocator.getProjectService().findAll(session));
    domainDto.setTask(serviceLocator.getTaskService().findAll(session));
    domainDto.setUser(serviceLocator.getUserService().findAll(session));
  }
}
