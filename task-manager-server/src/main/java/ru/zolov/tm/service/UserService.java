package ru.zolov.tm.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import lombok.Cleanup;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.IUserService;
import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.dto.UserDto;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserExistException;
import ru.zolov.tm.exception.UserNotFoundException;
import ru.zolov.tm.repository.UserRepository;
import ru.zolov.tm.util.HashUtil;

public class UserService extends AbstractService<User> implements IUserService {

  @NotNull private ServiceLocator serviceLocator;

  public UserService(@NotNull final ServiceLocator serviceLocator) {
    this.serviceLocator = serviceLocator;
  }

  @Override public User login(
      @Nullable final String login,
      @Nullable final String password
  ) throws EmptyStringException, UserNotFoundException, SQLException, EmptyRepositoryException {
    if (login == null || login.isEmpty()) throw new EmptyStringException();
    if (password == null || password.isEmpty()) throw new EmptyStringException();
    @NotNull @Cleanup EntityManager entityManager = serviceLocator.getEntityManagerFactory().createEntityManager();
    @NotNull UserRepository userRepository = new UserRepository(entityManager);
    @Nullable User user = userRepository.findByLogin(login);
    @NotNull final String passwordHash = HashUtil.md5(password);
    if (passwordHash == null) throw new UserNotFoundException();
    if (user == null) throw new UserNotFoundException();
    if (!passwordHash.equals(user.getPasswordHash())) throw new UserNotFoundException();
    return user;
  }

  @Override public String userRegistration(
      @Nullable final String login,
      @Nullable final String password
  ) throws EmptyStringException, UserExistException, SQLException, EmptyRepositoryException {
    if (login == null || login.isEmpty()) throw new EmptyStringException();
    if (password == null || password.isEmpty()) throw new EmptyStringException();
    @NotNull @Cleanup EntityManager entityManager = serviceLocator.getEntityManagerFactory().createEntityManager();
    @NotNull UserRepository userRepository = new UserRepository(entityManager);
    @Nullable User foundedOne = userRepository.findByLogin(login);
    if (foundedOne != null) throw new UserExistException();
    @NotNull final User user = new User();
    @Nullable String hashedPassword = HashUtil.md5(password);
    if (hashedPassword == null) throw new EmptyStringException();
    entityManager.getTransaction().begin();
    user.setLogin(login);
    user.setRole(RoleType.USER);
    user.setPasswordHash(hashedPassword);
    userRepository.persist(user);
    entityManager.getTransaction().commit();
    @NotNull final String userId = user.getId();
    return userId;
  }

  @Override public @NotNull List<User> findAll(@NotNull final Session session) throws EmptyRepositoryException, EmptyStringException, SQLException, AccessForbiddenException {
    if (session.getUser().getRole() == null) throw new AccessForbiddenException();
    if (session.getUser().getRole().name() != "ADMIN") throw new AccessForbiddenException();
    @NotNull @Cleanup EntityManager entityManager = serviceLocator.getEntityManagerFactory().createEntityManager();
    @NotNull UserRepository userRepository = new UserRepository(entityManager);
    return userRepository.findAll();
  }

  @Override public @Nullable User findOneById(@NotNull String id) throws UserNotFoundException, SQLException, EmptyStringException, EmptyRepositoryException {
    @NotNull @Cleanup EntityManager entityManager = serviceLocator.getEntityManagerFactory().createEntityManager();
    @NotNull UserRepository userRepository = new UserRepository(entityManager);
    @Nullable User user = userRepository.findById(id);
    if (user == null) throw new UserNotFoundException();
    return user;
  }

  @Override public void updateUserPassword(
      @Nullable final String id,
      @Nullable final String password
  ) throws EmptyStringException, UserNotFoundException, EmptyRepositoryException, SQLException {
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    if (password == null || password.isEmpty()) throw new EmptyStringException();
    @NotNull @Cleanup EntityManager entityManager = serviceLocator.getEntityManagerFactory().createEntityManager();
    @NotNull UserRepository userRepository = new UserRepository(entityManager);
    entityManager.getTransaction().begin();
    @Nullable User user = userRepository.findById(id);
    @NotNull String hashedPassword = HashUtil.md5(password);
    user.setPasswordHash(hashedPassword);
    userRepository.merge(user);
  }

  @Override public void load(
      @NotNull Session session,
      @Nullable List<User> list
  ) throws EmptyRepositoryException, EmptyStringException, SQLException, AccessForbiddenException {
    if (list == null) throw new EmptyRepositoryException();
    if (session.getUser().getRole() == null) throw new AccessForbiddenException();
    if (session.getUser().getRole().name() != "ADMIN") throw new AccessForbiddenException();
    @NotNull @Cleanup EntityManager entityManager = serviceLocator.getEntityManagerFactory().createEntityManager();
    @NotNull UserRepository userRepository = new UserRepository(entityManager);
    if (userRepository == null) throw new SQLException();
    entityManager.getTransaction().begin();
    for (@Nullable final User user : list) {
      userRepository.persist(user);
      entityManager.getTransaction().commit();
    }
  }

  @Override public UserDto transformUserToDto(@NotNull final User user) {
    @NotNull final UserDto userDto = new UserDto();
    userDto.setId(user.getId());
    userDto.setLogin(user.getLogin());
    userDto.setPasswordHash(user.getPasswordHash());
    userDto.setRole(user.getRole());
    return userDto;
  }

  @Override public List<UserDto> transformListOfUser(@NotNull final List<User> list) {
    List<UserDto> resultList = new ArrayList<>();
    for (@NotNull User user : list) {
      resultList.add(transformUserToDto(user));
    }
    return resultList;
  }
}
