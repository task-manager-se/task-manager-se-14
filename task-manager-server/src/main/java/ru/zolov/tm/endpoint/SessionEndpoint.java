package ru.zolov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.ISessionEndpoint;
import ru.zolov.tm.dto.SessionDto;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;

@NoArgsConstructor
@WebService(endpointInterface = "ru.zolov.tm.api.ISessionEndpoint")
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

  @SneakyThrows @Nullable @Override @WebMethod public SessionDto openSession(
      @NotNull @WebParam(name = "login") String login,
      @NotNull @WebParam(name = "password") String password
  ) {
    @NotNull final User current = serviceLocator.getUserService().login(login, password);
    @NotNull final Session newSession = serviceLocator.getSessionService().open(current);
    @NotNull final SessionDto newSessionDto = serviceLocator.getSessionService().transformSessionToDto(newSession, current);
    return newSessionDto;
  }


  @SneakyThrows @Override @WebMethod public void closeSesson(
      @NotNull @WebParam(name = "sessionDTO") SessionDto sessionDto
  ) {
    @NotNull final User user = serviceLocator.getUserService().findOneById(sessionDto.getUserId());
    @NotNull final Session session = serviceLocator.getSessionService().transformDtoToSession(sessionDto, user);
    serviceLocator.getSessionService().close(session);
  }
}
