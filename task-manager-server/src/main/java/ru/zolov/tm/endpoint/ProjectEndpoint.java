package ru.zolov.tm.endpoint;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.IProjectEndpoint;
import ru.zolov.tm.dto.ProjectDto;
import ru.zolov.tm.dto.SessionDto;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.exception.AccessForbiddenException;


@NoArgsConstructor
@WebService(endpointInterface = "ru.zolov.tm.api.IProjectEndpoint")
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

  @SneakyThrows @NotNull @Override @WebMethod public ProjectDto createNewProject(
      @NotNull @WebParam(name = "sessionDTO") final SessionDto sessionDto,
      @NotNull @WebParam(name = "name") final String projectName,
      @NotNull @WebParam(name = "description") final String projectDescription,
      @NotNull @WebParam(name = "start") final String dateOfStart,
      @NotNull @WebParam(name = "finish") final String dateOfFinish
  ) {
    @NotNull final User user = serviceLocator.getUserService().findOneById(sessionDto.getUserId());
    @NotNull final Session session = serviceLocator.getSessionService().transformDtoToSession(sessionDto, user);
    serviceLocator.getSessionService().validate(session);
    Project project = serviceLocator.getProjectService().create(user, projectName, projectDescription, dateOfStart, dateOfFinish);
    return serviceLocator.getProjectService().transformProjectToDto(project);
  }

  @SneakyThrows @NotNull @Override @WebMethod public List<ProjectDto> findAllProjectByUserId(
      @NotNull @WebParam(name = "sessionDTO") final SessionDto sessionDto
  ) {
    @NotNull final User user = serviceLocator.getUserService().findOneById(sessionDto.getUserId());
    @NotNull final Session session = serviceLocator.getSessionService().transformDtoToSession(sessionDto, user);
    serviceLocator.getSessionService().validate(session);
    List<Project> allByUserId = serviceLocator.getProjectService().findAllByUserId(sessionDto.getUserId());
    return serviceLocator.getProjectService().transformListProject(allByUserId);
  }

  @SneakyThrows @Nullable @Override @WebMethod public ProjectDto findProjectById(
      @NotNull @WebParam(name = "sessionDTO") final SessionDto sessionDto,
      @NotNull @WebParam(name = "id") String projectId
  ) {
    @NotNull final User user = serviceLocator.getUserService().findOneById(sessionDto.getUserId());
    @NotNull final Session session = serviceLocator.getSessionService().transformDtoToSession(sessionDto, user);
    serviceLocator.getSessionService().validate(session);
    Project byUserAndId = serviceLocator.getProjectService().findByUserAndId(sessionDto.getUserId(), projectId);
    return serviceLocator.getProjectService().transformProjectToDto(byUserAndId);
  }

  @SneakyThrows @Override @WebMethod public void removeProjectById(
      @NotNull @WebParam(name = "sessionDTO") final SessionDto sessionDto,
      @NotNull @WebParam(name = "id") String projectId
  ) {
    @NotNull final User user = serviceLocator.getUserService().findOneById(sessionDto.getUserId());
    @NotNull final Session session = serviceLocator.getSessionService().transformDtoToSession(sessionDto, user);
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getProjectService().removeById(sessionDto.getUserId(), projectId);
  }

  @SneakyThrows @Override @WebMethod public void updateProject(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto,
      @NotNull @WebParam(name = "id") final String projectId,
      @NotNull @WebParam(name = "name") final String projectName,
      @NotNull @WebParam(name = "description") final String projectDescription,
      @NotNull @WebParam(name = "start") final String start,
      @NotNull @WebParam(name = "finish") final String finish
  ) {
    @NotNull final User user = serviceLocator.getUserService().findOneById(sessionDto.getUserId());
    @NotNull final Session session = serviceLocator.getSessionService().transformDtoToSession(sessionDto, user);
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getProjectService().update(user, projectId, projectName, projectDescription, start, finish);
  }

  @SneakyThrows @NotNull @Override @WebMethod public List<ProjectDto> findProjectByPartOfTheName(
      @NotNull @WebParam(name = "sessionDTO") final SessionDto sessionDto,
      @NotNull @WebParam(name = "partOfTheName") final String partOfTheName
  ) {
    @NotNull final User user = serviceLocator.getUserService().findOneById(sessionDto.getUserId());
    @NotNull final Session session = serviceLocator.getSessionService().transformDtoToSession(sessionDto, user);
    serviceLocator.getSessionService().validate(session);
    List<Project> projects = serviceLocator.getProjectService().findProject(sessionDto.getUserId(), partOfTheName);
    return serviceLocator.getProjectService().transformListProject(projects);
  }
}
