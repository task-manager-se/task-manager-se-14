package ru.zolov.tm.endpoint;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.ITaskEndpoint;
import ru.zolov.tm.dto.SessionDto;
import ru.zolov.tm.dto.TaskDto;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.exception.AccessForbiddenException;


@NoArgsConstructor
@WebService(endpointInterface = "ru.zolov.tm.api.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

  @SneakyThrows @NotNull @Override @WebMethod public TaskDto addNewTask(
      @NotNull @WebParam(name = "sessionDTO") final SessionDto sessionDto,
      @NotNull @WebParam(name = "projectId") final String projectId,
      @NotNull @WebParam(name = "name") final String name,
      @NotNull @WebParam(name = "description") final String description,
      @NotNull @WebParam(name = "start") final String start,
      @NotNull @WebParam(name = "finish") final String finish
  )  {
    if (sessionDto.getUserId() == null) throw new AccessForbiddenException();
    @NotNull final User user = serviceLocator.getUserService().findOneById(sessionDto.getUserId());
    @NotNull final Session session = serviceLocator.getSessionService().transformDtoToSession(sessionDto, user);
    @NotNull final Project project = serviceLocator.getProjectService().findByUserAndId(user.getId(), projectId);
    Task task = serviceLocator.getTaskService().create(user, project, name, description, start, finish);
    serviceLocator.getSessionService().validate(session);
    return serviceLocator.getTaskService().transformTaskToDto(task);
  }

  @SneakyThrows @NotNull @Override @WebMethod public List<TaskDto> findAllTasksByUserId(
      @NotNull @WebParam(name = "sessionDTO") final SessionDto sessionDto
  ) {
    if (sessionDto.getUserId() == null) throw new AccessForbiddenException();
    @NotNull final User user = serviceLocator.getUserService().findOneById(sessionDto.getUserId());
    @NotNull final Session session = serviceLocator.getSessionService().transformDtoToSession(sessionDto, user);
    serviceLocator.getSessionService().validate(session);
    @NotNull final List<Task> taskList = serviceLocator.getTaskService().findAllByUserId(sessionDto.getUserId());
    return serviceLocator.getTaskService().transformListOfTask(taskList);
  }

  @SneakyThrows @NotNull @Override @WebMethod public List<TaskDto> findTasksByProjectId(
      @NotNull @WebParam(name = "sessionDTO") final SessionDto sessionDto,
      @NotNull @WebParam(name = "projectId") final String projectId
  ) {
    @NotNull final User user = serviceLocator.getUserService().findOneById(sessionDto.getUserId());
    @NotNull final Session session = serviceLocator.getSessionService().transformDtoToSession(sessionDto, user);
    serviceLocator.getSessionService().validate(session);
    List<Task> allByUserId = serviceLocator.getTaskService().findAllByUserId(sessionDto.getUserId());
    return serviceLocator.getTaskService().transformListOfTask(allByUserId);
  }

  @SneakyThrows @Nullable @Override @WebMethod public TaskDto findOneTaskById(
      @NotNull @WebParam(name = "sessionDTO") final SessionDto sessionDto,
      @NotNull @WebParam(name = "projectId") final String projectId,
      @NotNull @WebParam(name = "taskId") final String taskId
  ) {
    @NotNull final User user = serviceLocator.getUserService().findOneById(sessionDto.getUserId());
    @NotNull final Session session = serviceLocator.getSessionService().transformDtoToSession(sessionDto, user);
    serviceLocator.getSessionService().validate(session);
    @NotNull final Task task = serviceLocator.getTaskService().findTaskById(taskId);
    return serviceLocator.getTaskService().transformTaskToDto(task);
  }

  @SneakyThrows @Override @WebMethod public void removeOneTaskById(
      @NotNull @WebParam(name = "sessionDTO") final SessionDto sessionDto,
      @NotNull @WebParam(name = "taskId") final String taskId
  ) {
    @NotNull final User user = serviceLocator.getUserService().findOneById(sessionDto.getUserId());
    @NotNull final Session session = serviceLocator.getSessionService().transformDtoToSession(sessionDto, user);
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getTaskService().removeTaskById(sessionDto.getUserId(), taskId);
  }

  @SneakyThrows @Override @WebMethod public void updateTask(
      @NotNull @WebParam(name = "sessionDTO") final SessionDto sessionDto,
      @NotNull @WebParam(name = "taskId") final String taskId,
      @NotNull @WebParam(name = "taskName") final String taskName,
      @NotNull @WebParam(name = "taskDescription") final String taskDescription,
      @NotNull @WebParam(name = "dateOfstart") final String dateOfstart,
      @NotNull @WebParam(name = "dateOffinish") final String dateOffinish
  ) {
    @NotNull final User user = serviceLocator.getUserService().findOneById(sessionDto.getUserId());
    @NotNull final Session session = serviceLocator.getSessionService().transformDtoToSession(sessionDto, user);
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getTaskService().update(user, taskId, taskName, taskDescription, dateOfstart, dateOffinish);
  }

  @SneakyThrows @NotNull @Override @WebMethod public List<TaskDto> findTaskByPartOfTheName(
      @NotNull @WebParam(name = "sessionDTO") final SessionDto sessionDto,
      @NotNull @WebParam(name = "partOfTheName") final String partOfTheName
  ) {
    @NotNull final User user = serviceLocator.getUserService().findOneById(sessionDto.getUserId());
    @NotNull final Session session = serviceLocator.getSessionService().transformDtoToSession(sessionDto, user);
    serviceLocator.getSessionService().validate(session);
    @NotNull final List<Task> taskList = serviceLocator.getTaskService().findTask(sessionDto.getUserId(), partOfTheName);
    return serviceLocator.getTaskService().transformListOfTask(taskList);
  }

}
