package ru.zolov.tm.endpoint;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.IUserEndpoint;
import ru.zolov.tm.dto.SessionDto;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserExistException;


@NoArgsConstructor
@WebService(endpointInterface = "ru.zolov.tm.api.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

  @Override @WebMethod public void registerNewUser(
      @NotNull @WebParam(name = "login") final String login,
      @NotNull @WebParam(name = "password") final String password
  ) throws EmptyStringException, UserExistException, EmptyRepositoryException, SQLException {
    serviceLocator.getUserService().userRegistration(login, password);
  }

  @SneakyThrows @Override @WebMethod public @NotNull void updateUserPassword(
      @NotNull @WebParam(name = "sessionDTO") final SessionDto sessionDto,
      @NotNull @WebParam(name = "newPassword") final String newPassword
  ) {
    @NotNull final User user = serviceLocator.getUserService().findOneById(sessionDto.getUserId());
    @NotNull Session session = serviceLocator.getSessionService().transformDtoToSession(sessionDto, user);
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getUserService().updateUserPassword(session.getUser().getId(), newPassword);
  }

  @SneakyThrows @Override @WebMethod public boolean isRolesAllowed(
      @NotNull @WebParam(name = "sessionDTO") final Session session,
      @Nullable @WebParam(name = "roleType") final RoleType... roleTypes
  ) {
    serviceLocator.getSessionService().validate(session);
    if (roleTypes == null) return false;
    final List<RoleType> types = Arrays.asList(roleTypes);
    return types.contains(session.getUser().getRole());
  }
}
