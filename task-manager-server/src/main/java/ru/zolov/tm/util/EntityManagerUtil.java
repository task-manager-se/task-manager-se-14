package ru.zolov.tm.util;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManagerFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.entity.User;

public class EntityManagerUtil {
  public static EntityManagerFactory getFactory() {
    final Map<String, String> settings = new HashMap<>();
    settings.put(Environment.DRIVER, PropertyUtil.MYSQL_DRIVER);
    settings.put(Environment.URL, PropertyUtil.MYSQL_URL);
    settings.put(Environment.USER, PropertyUtil.MYSQL_USER);
    settings.put(Environment.PASS, PropertyUtil.MYSQL_PASSWORD);
    settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
    settings.put(Environment.HBM2DDL_AUTO, "update");
    settings.put(Environment.SHOW_SQL, "true");


    final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
    registryBuilder.applySettings(settings);
    final StandardServiceRegistry registry = registryBuilder.build();
    final MetadataSources sources = new MetadataSources(registry);
    sources.addAnnotatedClass(Task.class);
    sources.addAnnotatedClass(Project.class);
    sources.addAnnotatedClass(User.class);
    sources.addAnnotatedClass(Session.class);
    final Metadata metadata = sources.getMetadataBuilder().build();
    return metadata.getSessionFactoryBuilder().build();
  }
}
