package ru.zolov.tm.util;

import java.util.Date;
import java.util.List;
import java.util.Scanner;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.ITerminalService;
import ru.zolov.tm.dto.AbstractEntityDto;
import ru.zolov.tm.dto.AbstractGoalDto;
import ru.zolov.tm.entity.AbstractEntity;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.service.AbstractService;


public class TerminalUtil extends AbstractService<AbstractEntity> {


  private static final Scanner scanner = new Scanner(System.in);

  public static String nextLine() {
    return scanner.nextLine();
  }

  public static Integer nextInt() {
    return scanner.nextInt();
  }

}